//
//  main.c
//  CommunicationArduino
//
//  Created by Megha Kalia, grad2021-msl-v on 11/16/21.
//  Copyright © 2021 Megha Kalia, grad2021-msl-v. All rights reserved.
//

/* www.chrisheydrick.com
 
 June 23 2012
 CanonicalArduinoRead write a byte to an Arduino, and then
 receives a serially transmitted string in response.
 The call/response Arduino sketch is here:
 https://gist.github.com/2980344
 Arduino sketch details at www.chrisheydrick.com
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <sys/ioctl.h>

#define DEBUG 1

int main(int argc, const char * argv[]) {
    int fd, n;
    int megha_triggerVal1 = 3 ;
    int megha_triggerVal2 = 4 ;
    char buf[100] = {0};
//    char buf[9] = "temp text";
    struct termios toptions;
    
    /* open serial port */
    fd = open("/dev/cu.usbmodem141201", O_RDWR | O_NOCTTY);
    printf("fd opened as %i\n", fd);
    
    /* wait for the Arduino to reboot */
    usleep(3500000); //this seems crucial otherwise no bytes are read
    
    /* get current serial port settings */
    tcgetattr(fd, &toptions);
    /* set 9600 baud both ways */
    cfsetispeed(&toptions, B57600);
    cfsetospeed(&toptions, B57600);
    /* 8 bits, no parity, no stop bits */
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    /* Canonical mode */
    toptions.c_lflag |= ICANON;
    /* commit the serial port settings */
    tcsetattr(fd, TCSANOW, &toptions);
    
    
    /* Send byte to trigger Arduino to send string back */
//    write(fd, "!", 1);
    /* Receive string from Arduino */
//    n = read(fd, buf, 64);
    
    
//    //testing writing an integer
//
    // Serial communication
    sprintf(buf, "%d,%d", megha_triggerVal1, megha_triggerVal2); //trying to send data as comma separated values in arduino
    int ret = write(fd, buf, 3); //
    printf("%d : the number of bytes in the buffer \n", strlen(buf));
    
    /* insert terminating zero in the string */
//    buf[n] = 0;
    
//    printf("%i bytes read, buffer contains: %s\n", n, buf);
    
    
    return 0;
}
